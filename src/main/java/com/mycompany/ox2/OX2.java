/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.ox2;
import java.util.Scanner;
/**
 *
 * @author ktmet
 */
public class OX2 {
    private static char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    private static char turn = 'O';
    private static Scanner sc = new Scanner(System.in);
    private static int row;
    private static int col;
    private static int count = 0;

public static void main(String[] args) {
    printWelcome();
        while(true) {
            showTable();
            showtrue();
            inputRowCol();
            if(isFinish()){
                showTable();
                showResult();
                break;
            }
            switchTurn();
    }
}
    private static void printWelcome() {
        System.out.println("Welcome to OX game");
    }
    private static void showTable(){
        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
              System.out.print(table[row][col] + " ");
            }
            System.out.println();
        }
    }
    private static void showtrue() {
        System.out.println("Turn " + turn);
    }
    public static void inputRowCol() {
        System.out.println("Plese input row, col: ");
        row = sc.nextInt() - 1;
        col = sc.nextInt() - 1;
        // System.out.println("" + row + " " + col);
        table[row][col] = turn;
        count++;
    }
    private static void switchTurn() {
        if(turn == 'X') {
            turn = 'O';
        } else {
            turn = 'X';
        }
    }
    private static boolean isFinish() {
        if(checkWin()){
            return true;
        }
        if(checkDraw()) {
            return true;
        }
        return false;
    }
    private static boolean checkWin() { 
        if(checkRow()){
            return true;
        }
        if(checkCol()){
            return true;
        }
        if(checkX()){
            return true;
        }
        return false;
    }
    private static boolean checkDraw() {
        if(count == 9) {
            return true;
        }
        return false;
    }
    private static boolean checkRow() {
        if (table[row][0] == turn && table[row][1] == turn && table[row][2] == turn ) {
            return true;
        }
        return false;
    }
    private static boolean checkCol() {
        if (table[0][col] == turn && table[1][col] == turn && table[2][col] == turn ) {
            return true;
        }
        return false;
    }
    private static boolean checkX() {
        if (table[0][0] == turn && table[1][1] == turn && table[2][2] == turn) {
            return true;
          }
      
        if (table[0][2] == turn && table[1][1] == turn && table[2][0] == turn) {
            return true;
          }
        return false;
    }
    private static void showResult(){
        if(checkWin()){
            System.out.println(turn + " Win !!!");
        }
        if(checkDraw()){
            System.out.println(" Drew !!!");
        }
    }
}